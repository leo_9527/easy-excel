package com.husky.easyexcle.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author husky
 * @Date 2022/3/16 16:43
 * @Description: 导出时追加指定字符串
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExportAppendStringAnnotation {

    /**
     * 开始位置追加内容
     * @return
     */
    public String beginPositionContent() default "";

    /**
     * 结束位置追加内容
     * @return
     */
    public String endPositionContent() default "";


}
