package com.husky.easyexcle.interceptor;

import com.alibaba.excel.util.BooleanUtils;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.SheetWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import com.alibaba.excel.write.handler.context.SheetWriteHandlerContext;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteWorkbookHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;

/**
 * @Author husky
 * @Date 2023/2/14 10:13
 * @Description: 自定义工作簿导出拦截器
 **/
@Slf4j
public class CustomSheetWriteHandler implements SheetWriteHandler {

    /**
     * 创建工作簿之后调用
     * @param context
     */
    @Override
    public void afterSheetCreate(SheetWriteHandlerContext context) {
        log.info("第{}个Sheet写入成功。", context.getWriteSheetHolder().getSheetNo());

        // 区间设置 第一列第一行和第二行的数据。由于第一行是头，所以第一、二行的数据实际上是第二三行
        // CellRangeAddressList 实现单元格范围地址列表
        CellRangeAddressList cellRangeAddressList = new CellRangeAddressList(1, 2, 0, 0);

        // DataValidationHelper 数据校验辅助类
        DataValidationHelper helper = context.getWriteSheetHolder().getSheet().getDataValidationHelper();




        // DataValidationConstraint 数据校验约束
        DataValidationConstraint constraint = helper.createExplicitListConstraint(new String[] {"下拉框测试1", "下拉框测试2"});
        DataValidation dataValidation = helper.createValidation(constraint, cellRangeAddressList);
        // 阻止输入非下拉选项的值
        // DataValidation 错误框的错误样式
        dataValidation.setErrorStyle(DataValidation.ErrorStyle.STOP);

        // setShowErrorBox(true) 设置输入无效值时的行为
        dataValidation.setShowErrorBox(true);

        // setSuppressDropDownArrow 根据列表验证对象
        dataValidation.setSuppressDropDownArrow(true);

        // 设置错误框的标题和文本。当用户在属于该*验证对象的单元格中输入无效值时，将显示错误框。为了显示一个错误框，你还应该使用setShowErrorBox方法(boolean show)
        dataValidation.createErrorBox("提示","此值与单元格定义格式不一致");

        // 设置提示框的标题和文本。当*用户选择属于该验证对象的单元格时，将显示提示框。为了显示一个提示框，你还应该使用方法setShowPromptBox(boolean show)
        dataValidation.createPromptBox("填写说明：","填写内容只能为下拉数据集中的单位，其他单位将会导致无法入仓");

        // 设置选择属于此对象的单元格时的行为
        dataValidation.setShowPromptBox(true);

        context.getWriteSheetHolder().getSheet().addValidationData(dataValidation);
    }
}
