package com.husky.easyexcle.interceptor;

import com.alibaba.excel.util.BooleanUtils;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;

/**
 * @Author husky
 * @Date 2023/2/14 10:13
 * @Description: 自定义单元格导出拦截器
 **/
@Slf4j
public class CustomCellWriteHandler implements CellWriteHandler {

    /**
     * 在单元格上的所有操作完成后调用
     * 对第一行第一列的头超链接到:https://gitee.com/leo_9527/easy-excel
     * @param context
     */
    @Override
    public void afterCellDispose(CellWriteHandlerContext context) {
        Cell cell = context.getCell();
        // 这里可以对cell进行任何操作
        log.info("第{}行，第{}列写入完成。", cell.getRowIndex(), cell.getColumnIndex());

        // 有数据 并且是 第一列
        if (BooleanUtils.isTrue(context.getHead()) && cell.getColumnIndex() == 0) {
            // 获取 poi HSSF和XSSF的辅助类
            CreationHelper createHelper = context.getWriteSheetHolder().getSheet().getWorkbook().getCreationHelper();

            // 创建一个超链接
            Hyperlink hyperlink = createHelper.createHyperlink(HyperlinkType.URL);
            hyperlink.setAddress("https://gitee.com/leo_9527/easy-excel");
            cell.setHyperlink(hyperlink);
        }
    }

}
