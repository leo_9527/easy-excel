package com.husky.easyexcle.utils;

/**
 * @Author husky
 * @Date 2022/7/11 10:01
 * @Description:
 **/
public enum DateToolsEnum{
    /**
     * yyyy-MM-dd HH:mm:ss
     */
    YYYY_MM_DD_HH_MM_SS_1(
            "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$"
            ,"yyyy-MM-dd HH:mm:ss"),
    /**
     * yyyy-MM-ddTHH:mm:ss
     */
    YYYY_MM_DD_HH_MM_SS_2(
            "^[1-9]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$"
            ,"yyyy/MM/dd HH:mm:ss"),
    /**
     * yyyyMMddHHmmss
     */
    YYYYMMDDHHMMSS(
            "^[1-9]\\d{3}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])(20|21|22|23|[0-1]\\d)[0-5]\\d[0-5]\\d$"
            ,"yyyyMMddHHmmss"),

    /**
     * MM-dd-yyyy HH:mm:ss
     */
    MM_DD_YYYY_HH_MM_SS_1(
            "^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-[1-9]\\d{3}\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$"
            ,"MM-dd-yyyy HH:mm:ss"),
    /**
     * MM/dd/yyyy HH:mm:ss
     */
    MM_DD_YYYY_HH_MM_SS_2(
            "^(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])/[1-9]\\d{3}\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$"
            ,"MM/dd/yyyy HH:mm:ss"),

    /**
     * yyyy-MM-dd
     */
    YYYY_MM_DD_1(
            "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$"
            ,"yyyy-MM-dd"),
    /**
     * yyyy-MM-dd
     */
    YYYY_MM_DD_2(
            "^[1-9]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])$"
            ,"yyyy/MM/dd"),
    /**
     * yyyyMMdd
     */
    YYYYMMDD(
            "^[1-9]\\d{3}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$"
            ,"yyyyMMdd"),
    ;
    /**
     * 正则表达式
     */
    private String reg;

    /**
     * 格式化类型
     */
    private String format;

    private DateToolsEnum(String reg,String format){
        this.reg = reg;
        this.format = format;
    }

    public String getReg() {
        return reg;
    }

    public String getFormat() {
        return format;
    }

}
