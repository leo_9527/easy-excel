package com.husky.easyexcle.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Author husky
 * @Date 2022/6/14 9:01
 * @Description: 时间工具
 **/
public class DateTools {



    /**
     * 字符串转 LocalDateTime
     * @param dateStr 时间格式字符串
     * @return
     */
    public static LocalDateTime stringToLocalDateTime(String dateStr){

        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_2.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_2.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.YYYYMMDDHHMMSS.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDDHHMMSS.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_1.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_1.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_2.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_2.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_1.getReg())){
            LocalDate localDate = LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_1.getFormat()));
            return  LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
        }
        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_2.getReg())){
            LocalDate localDate = LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_2.getFormat()));
            return  LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
        }
        if(dateStr.matches(DateToolsEnum.YYYYMMDD.getReg())){
            LocalDate localDate = LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDD.getFormat()));
            return  LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
        }
       throw new RuntimeException("字符串转 LocalDateTime 异常，未找到匹配格式。待转换字符串:"+dateStr);
    }

    /**
     * 字符串转 LocalDate
     * @param dateStr 时间格式字符串
     * @return
     */
    public static LocalDate stringToLocalDate(String dateStr){

        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat())).toLocalDate();
        }
        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_2.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_2.getFormat())).toLocalDate();
        }
        if(dateStr.matches(DateToolsEnum.YYYYMMDDHHMMSS.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDDHHMMSS.getFormat())).toLocalDate();
        }
        if(dateStr.matches(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_1.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_1.getFormat())).toLocalDate();
        }
        if(dateStr.matches(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_2.getReg())){
            return LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_2.getFormat())).toLocalDate();
        }
        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_1.getReg())){
            return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_1.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.YYYY_MM_DD_2.getReg())){
            return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_2.getFormat()));
        }
        if(dateStr.matches(DateToolsEnum.YYYYMMDD.getReg())){
            return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDD.getFormat()));
        }
        throw new RuntimeException("字符串转 LocalDate 异常，未找到匹配格式。待转换字符串:"+dateStr);
    }

    /**
     * LocalDateTime 转 字符串
     * @param localDateTime
     * @param dateToolsEnum 待转换格式
     * @return
     */
    public static String localDateTimeToString(LocalDateTime localDateTime,DateToolsEnum dateToolsEnum){

        if(dateToolsEnum.equals(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1)){
           return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_2)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_2.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYYMMDDHHMMSS)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDDHHMMSS.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_1)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_1.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_2)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.MM_DD_YYYY_HH_MM_SS_2.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYY_MM_DD_1)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_1.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYY_MM_DD_2)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_2.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYYMMDD)){
            return localDateTime.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDD.getFormat()));
        }else {
            throw new RuntimeException(" LocalDateTime 转 字符串 异常，未找到匹配格式。待转换字符串格式:"+dateToolsEnum.getFormat());
        }
    }

    /**
     * LocalDate 转 字符串
     * @param localDate
     * @param dateToolsEnum 待转换格式
     * @return
     */
    public static String localDateToString(LocalDate localDate,DateToolsEnum dateToolsEnum){
        if(dateToolsEnum.equals(DateToolsEnum.YYYY_MM_DD_1)){
            return localDate.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_1.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYY_MM_DD_2)){
            return localDate.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYY_MM_DD_2.getFormat()));
        }else if(dateToolsEnum.equals(DateToolsEnum.YYYYMMDD)){
            return localDate.format(DateTimeFormatter.ofPattern(DateToolsEnum.YYYYMMDD.getFormat()));
        }else {
            throw new RuntimeException(" LocalDate 转 字符串 异常，未找到匹配格式。待转换字符串格式:"+dateToolsEnum.getFormat());
        }
    }
    /**
     * 时间格式字符串 转 格式化字符串
     * @param dateStr 时间格式字符串
     * @param dateToolsEnum 待转换格式
     * @return
     */
    public static String dateStrToFormatStr(String dateStr,DateToolsEnum dateToolsEnum){
        try {
            LocalDateTime localDateTime = DateTools.stringToLocalDateTime(dateStr);
            return localDateTime.format(DateTimeFormatter.ofPattern(dateToolsEnum.getFormat()));
        }catch (Exception e){
            throw new RuntimeException(" LocalDateTime 转 字符串 异常，未找到匹配格式。待转换字符串:"+dateStr+" 待转换字符串格式:"+dateToolsEnum.getFormat());
        }
    }

    /**
     * LocalDate 转 Date
     * @param localDate
     * @return
     */
    public static Date localDateToDate(LocalDate localDate) throws ParseException {
        LocalDateTime localDateTime = LocalDateTime.of(localDate, LocalTime.MIDNIGHT);
        String dateStr = DateTools.localDateTimeToString(localDateTime, DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1);
        return new SimpleDateFormat(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat()).parse(dateStr);
    }

    /**
     * LocalDateTime 转 Date
     * @param localDateTime
     * @return
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) throws ParseException {
        String dateStr = DateTools.localDateTimeToString(localDateTime, DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1);
        return new SimpleDateFormat(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat()).parse(dateStr);
    }

    /**
     * Date 转 LocalDateTime
     * @param date
     * @return
     */
    public static LocalDateTime dateToLocalDateTime(Date date) throws ParseException {
        String dateStr = new SimpleDateFormat(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat()).format(date);
        return DateTools.stringToLocalDateTime(dateStr);
    }

    /**
     * Date 转 LocalDate
     * @param date
     * @return
     */
    public static LocalDate dateToLocalDate(Date date) throws ParseException {
        String dateStr = new SimpleDateFormat(DateToolsEnum.YYYY_MM_DD_HH_MM_SS_1.getFormat()).format(date);
        return DateTools.stringToLocalDateTime(dateStr).toLocalDate();
    }

    public static void main(String[] args) {
        System.out.println(stringToLocalDateTime("20200101"));
        System.out.println(localDateTimeToString(LocalDateTime.now(), DateToolsEnum.YYYYMMDD));
        System.out.println(localDateToString(LocalDate.now(), DateToolsEnum.YYYY_MM_DD_1));
        System.out.println(dateStrToFormatStr("20200101", DateToolsEnum.YYYYMMDD));
        try {
            System.out.println(localDateToDate(LocalDate.now()));
            System.out.println(localDateTimeToDate(LocalDateTime.now()));
            System.out.println(dateToLocalDateTime(new Date()));
            System.out.println(dateToLocalDate(new Date()));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
