package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/16 16:57
 * @Description: 指定列的下标或者列名  读取 Excel 对应实体类
 * @ColumnWidth(50) 设置列宽
 * @HeadRowHeight(35) 设置行高
 * @HeadFontStyle 设置 字体大小、名称、颜色、加粗
 **/

@Data
public class ReadExcelIndexOrNameEntity {

    /**
     * 字符串类型的标题
     * 强制读取第三个 这里不建议 index 和 name[value] 同时用，要么一个对象只用index，要么一个对象只用name[value]去匹配
     */
    // @ExcelProperty("字符串标题")
    @ExcelProperty(index = 0)
    private String stringData;

    /**
     * 日期标题
     */
    // @ExcelProperty("日期标题")
    @ExcelProperty(index = 1)
    private LocalDateTime dateData;

    /**
     * 数字标题
     */
    // @ExcelProperty("数字标题")
    @ExcelProperty(index = 2)
    private double doubleData;
}
