package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.alibaba.excel.enums.BooleanEnum;
import com.alibaba.excel.metadata.data.WriteCellData;
import lombok.Data;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/13 13:19
 * @Description: 复杂内容 写 Excel 对应实体类
 * @ColumnWidth(50) 设置列宽
 * @HeadRowHeight(35) 设置行高
 * @HeadFontStyle 设置 字体大小、名称、颜色、加粗
 **/

@Data
@ColumnWidth(50)
@HeadRowHeight(35)
@HeadFontStyle(fontHeightInPoints = 14,fontName = "微软雅黑",color = 2 ,bold = BooleanEnum.TRUE)
public class WriteExcelComplexContentEntity {
    /**
     * 超链接
     *
     */
    @ExcelProperty(value = "超链接")
    private WriteCellData<String> hyperlink;

    /**
     * 备注
     *
     */
    @ExcelProperty(value = "备注")
    private WriteCellData<String> commentData;

    /**
     * 公式
     *
     */
    @ExcelProperty(value = "公式")
    private WriteCellData<String> formulaData;

    /**
     * 指定单元格的样式。当然样式 也可以用注解等方式。
     *
     */
    @ExcelProperty("单元格样式")
    private WriteCellData<String> writeCellStyle;

    /**
     * 指定一个单元格有多个样式
     *
     */
    @ExcelProperty("单元格多个样式")
    private WriteCellData<String> richText;

}
