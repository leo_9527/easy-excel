package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.enums.BooleanEnum;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/16 16:57
 * @Description: 最简单的 读取 Excel 对应实体类
 **/

@Data
public class ReadExcelBasicEntity {

    /**
     * 字符串类型的标题
     */
    private String stringData;

    /**
     * 日期标题
     */
    private LocalDateTime dateData;

    /**
     * 数字标题
     */
    private double doubleData;

}
