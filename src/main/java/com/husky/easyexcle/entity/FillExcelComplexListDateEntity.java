package com.husky.easyexcle.entity;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @Author husky
 * @Date 2023/2/21 15:50
 * @Description: 复杂列表数据
 **/
@Data
public class FillExcelComplexListDateEntity {
    /**
     * 姓名
     */
    private String name;

    /**
     * 年级
     */
    private Integer grade;
    /**
     * 成绩
     */
    private BigDecimal score;
}
