package com.husky.easyexcle.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author husky
 * @Date 2023/2/21 15:13
 * @Description: 小数据复杂模板填充 实体类
 **/
@Data
public class FillExcelComplexEntity {

    /**
     * 复杂列表数据
     */
    private List<FillExcelComplexListDateEntity> dateList;

    /**
     * 总成绩
     */
    private BigDecimal totalScore;

    /**
     * 创建时间
     */
    private LocalDateTime dateTime;

}


