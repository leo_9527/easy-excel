package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.enums.BooleanEnum;
import com.husky.easyexcle.annotation.ExportAppendStringAnnotation;
import com.husky.easyexcle.annotation.StringToBooleanConverterAnnotation;
import com.husky.easyexcle.converter.CustomExportAppendStringConverter;
import com.husky.easyexcle.converter.CustomStringToBooleanConverter;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/13 16:10
 * @Description: 自定义转换相关 写 Excel 对应实体类
 * @ColumnWidth(50) 设置列宽
 * @HeadRowHeight(35) 设置行高
 * @HeadFontStyle 设置 字体大小、名称、颜色、加粗
 **/

@Data
@ColumnWidth(50)
@HeadRowHeight(35)
@HeadFontStyle(fontHeightInPoints = 14,fontName = "微软雅黑",color = 10 ,bold = BooleanEnum.TRUE)
public class WriteExcelConverterEntity {

    /**
     * 字符串类型的标题
     */
    @ExportAppendStringAnnotation(beginPositionContent = "开头：",endPositionContent = "。已结束")
    @ExcelProperty(value = "字符串标题", converter = CustomExportAppendStringConverter.class)
    private String stringData;

    /**
     * 日期标题
     */
    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
    @ExcelProperty(value = "日期标题")
    private LocalDateTime dateData;

    /**
     * 数字标题
     */
    @NumberFormat("#.##%")
    @ExcelProperty(value = "数字标题")
    private double doubleData;

    @StringToBooleanConverterAnnotation(trueStr = "男",falseStr = "女")
    @ExcelProperty(value = "性别",converter = CustomStringToBooleanConverter.class)
    private boolean booleanData;
}
