package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.data.CellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/16 16:57
 * @Description: 额外信息（批注、超链接、合并单元格信息） 读取 Excel 对应实体类
 **/

@Data
public class ReadExcelExtraEntity {

    /**
     * 超链接
     */
    @ExcelProperty(index = 0)
    private String hyperlinkData;

    /**
     * 备注
     */
    @ExcelProperty(index = 1)
    private String commentData;

    /**
     * 官方：这里并不一定能完美的获取 有些公式是依赖性的 可能会读不到 这个问题后续会修复
     * 公式
     */
    @ExcelProperty(index = 2)
    private CellData<String> formulaDate;

    /**
     * 合并单元格
     */
    @ExcelProperty(index = 3)
    private String mergeData;


}
