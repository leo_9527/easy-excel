package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import com.alibaba.excel.enums.BooleanEnum;
import com.alibaba.excel.enums.poi.FillPatternTypeEnum;
import lombok.Data;
import org.apache.poi.ss.usermodel.FillPatternType;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/13 13:19
 * @Description: 注解样式的使用 写 Excel 对应实体类
 * @ColumnWidth(50) 设置列宽
 * @HeadRowHeight(35) 设置行高
 * @HeadFontStyle 设置 字体大小、名称、颜色、加粗
 **/

@Data
@ColumnWidth(50)
@HeadRowHeight(35)
@HeadFontStyle(fontHeightInPoints = 14,fontName = "微软雅黑",color = 2 ,bold = BooleanEnum.TRUE)
public class WriteExcelStyleEntity {

    // 字符串的头背景设置成粉红 IndexedColors.PINK.getIndex()
    @HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 14)
    // 字符串的头字体设置成20
    @HeadFontStyle(fontHeightInPoints = 30)
    // 字符串的内容的背景设置成天蓝 IndexedColors.SKY_BLUE.getIndex()
    @ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 40)
    // 字符串的内容字体设置成20
    @ContentFontStyle(fontHeightInPoints = 30)

    /**
     * 字符串类型的标题
     */
    @ExcelProperty(value = "字符串标题")
    private String stringData;

    /**
     * 日期标题
     */
    @ExcelProperty(value = "日期标题")
    private LocalDateTime dateData;

    /**
     * 数字标题
     * ColumnWidth(20) //宽度为20
     */

    @ColumnWidth(20)
    @ExcelProperty(value = "数字标题")
    private double doubleData;

}
