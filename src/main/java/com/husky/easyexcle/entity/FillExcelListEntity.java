package com.husky.easyexcle.entity;

import lombok.Data;

/**
 * @Author husky
 * @Date 2023/2/21 15:13
 * @Description: 列表模板填充 实体类
 **/
@Data
public class FillExcelListEntity {

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;
    /**
     * 地址
     */
    private String address;

}
