package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.enums.BooleanEnum;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/13 13:19
 * @Description: 分配列号 写 Excel 对应实体类
 * @ColumnWidth(50) 设置列宽
 * @HeadRowHeight(35) 设置行高
 * @HeadFontStyle 设置 字体大小、名称、颜色、加粗
 **/

@Data
@ColumnWidth(50)
@HeadRowHeight(35)
@HeadFontStyle(fontHeightInPoints = 14,fontName = "微软雅黑",color = 2 ,bold = BooleanEnum.TRUE)
public class WriteExcelAssignIndexEntity {

    /**
     * 字符串类型的标题
     */
    @ExcelProperty(value = "字符串标题",index = 0)
    private String stringData;

    /**
     * 日期标题
     */
    @ExcelProperty(value = "日期标题",index = 2)
    private LocalDateTime dateData;

    /**
     * 数字标题
     */
    @ExcelProperty(value = "数字标题",index = 5)
    private double doubleData;

}
