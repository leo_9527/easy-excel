package com.husky.easyexcle.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.enums.BooleanEnum;
import com.husky.easyexcle.annotation.StringToBooleanConverterAnnotation;
import com.husky.easyexcle.converter.CustomStringToBooleanConverter;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * @Author husky
 * @Date 2023/2/13 13:19
 * @Description: 复杂头 写 Excel 对应实体类
 * @ColumnWidth(50) 设置列宽
 * @HeadRowHeight(35) 设置行高
 * @HeadFontStyle 设置 字体大小、名称、颜色、加粗
 **/

@Data
@ColumnWidth(50)
@HeadRowHeight(35)
@HeadFontStyle(fontHeightInPoints = 14,fontName = "微软雅黑",color = 2 ,bold = BooleanEnum.TRUE)
public class WriteExcelComplexHeadEntity {

    /**
     * 字符串类型的标题
     */
    @ExcelProperty(value = {"一级分类","二级分类1","字符串标题1"})
    private String stringData1;

    /**
     * 字符串类型的标题
     */
    @ExcelProperty(value = {"一级分类","二级分类1","字符串标题2"})
    private String stringData2;

    /**
     * 字符串类型的标题
     */
    @ExcelProperty(value = {"一级分类","二级分类2","字符串标题3"})
    private String stringData3;
    /**
     * 字符串类型的标题
     */
    @ExcelProperty(value = {"一级分类","二级分类2","字符串标题4"})
    private String stringData4;

    /**
     * 日期标题
     */
    @ExcelProperty(value = {"统一分类","日期标题1"})
    private LocalDateTime dateData1;

    /**
     * 日期标题
     */
    @ExcelProperty(value = {"统一分类","日期标题2"})
    private LocalDateTime dateData2;

    /**
     * 数字标题
     */
    @ExcelProperty(value = "数字标题")
    private double doubleData;


}
