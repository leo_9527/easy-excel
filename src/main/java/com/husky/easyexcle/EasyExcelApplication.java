package com.husky.easyexcle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author husky
 * @Date 2023/2/16 17:12
 * @Description: 启动类
 **/
@SpringBootApplication
public class EasyExcelApplication {
    public static void main(String[] args) {
        SpringApplication.run(EasyExcelApplication.class,args);
    }
}
