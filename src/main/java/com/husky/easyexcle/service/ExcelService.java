package com.husky.easyexcle.service;

import com.husky.easyexcle.entity.ExcelDateEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

/**
 * @Author husky
 * @Date 2022/3/22 16:08
 * @Description: Excelweb相关业务接口
 **/
public interface ExcelService {

    /**
     * 读取上传Excel数据
     * @param file
     * @return
     */
    List<ExcelDateEntity> readExcelData(MultipartFile file);
}
