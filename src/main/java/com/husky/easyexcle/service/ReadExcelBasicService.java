package com.husky.easyexcle.service;

import com.husky.easyexcle.entity.ReadExcelBasicEntity;

import java.util.List;

/**
 * @Author husky
 * @Date 2023/2/16 17:55
 * @Description:
 **/
public interface ReadExcelBasicService {
    /**
     * 保存数据
     * @param cachedDataList
     */
    void save(List<ReadExcelBasicEntity> cachedDataList);
}
