package com.husky.easyexcle.service.impl;

import com.husky.easyexcle.entity.ExcelDateEntity;
import com.husky.easyexcle.service.ExcelService;
import com.husky.easyexcle.utils.ExcleUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

/**
 * @Author husky
 * @Date 2022/3/22 16:09
 * @Description: Excelweb相关业务实现类
 **/
@Slf4j
@Service
public class ExcelSerivceImpl implements ExcelService {

    /**
     * 读取上传Excel数据
     * @param file
     * @return
     */
    @Override
    public List<ExcelDateEntity> readExcelData(MultipartFile file) {
        return ExcleUtil.readWebExcelData(file, ExcelDateEntity.class);
    }
}
