package com.husky.easyexcle.service.impl;

import com.husky.easyexcle.entity.ReadExcelBasicEntity;
import com.husky.easyexcle.service.ReadExcelBasicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author husky
 * @Date 2023/2/16 17:57
 * @Description:
 **/
@Service
@Slf4j
public class ReadExcelBasicServiceImpl implements ReadExcelBasicService {

    /**
     * 数据存储相关业务
     * @param cachedDataList 待存储数据
     */
    @Override
    public void save(List<ReadExcelBasicEntity> cachedDataList) {
        // 此处为存储数据相关业务
        log.info("待存储数据={}",cachedDataList);
    }
}
