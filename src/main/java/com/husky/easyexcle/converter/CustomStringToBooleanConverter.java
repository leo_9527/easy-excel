package com.husky.easyexcle.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.husky.easyexcle.annotation.StringToBooleanConverterAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

/**
 * @Author husky
 * @Date 2022/3/15 10:52
 * @Description: 自定义字符串转布尔值转换器
 **/
@Slf4j
public class CustomStringToBooleanConverter  implements Converter<Boolean> {
    @Override
    public Class<?> supportJavaTypeKey() {
        return String.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    /**
     * 这里是读的时候会调用
     *
     * @return
     */
    @Override
    public Boolean convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        // 默认返回false
        boolean result = false;
        try{

            //获取属性上 StringToBooleanConverterAnnotation 注解
            StringToBooleanConverterAnnotation annotation = contentProperty.getField().getAnnotation(StringToBooleanConverterAnnotation.class);

            //判断该注解是否存在
            if(annotation != null){

                //获取Excle读取到的字符串类型的值
                String stringValue = cellData.getStringValue();

                //Excle读取到值真实有效
                if(!ObjectUtils.isEmpty(stringValue)){

                    //判断注解为true时的字符串 是否与 Excle读取到值相同
                    boolean srtFlag = annotation.trueStr().equals(stringValue);
                    if(srtFlag){

                        //相同返回true
                        result = true;
                    }
                }
            }else{
                log.error("使用自定义字符串转布尔值转换器，缺少配套注解@StringToBooleanConverterAnnotation");
            }
        }catch (Exception e){
            log.error("使用自定义字符串转布尔值转换器，获取属性反射异常。Exception={}",e);
        }
        return result;
    }

    /**
     * 这里是写的时候会调用
     * @return
     */
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Boolean> context) {

        // 默认返回空字符串
        String result = "";
        try {
            //获取属性上 StringToBooleanConverterAnnotation 注解
            StringToBooleanConverterAnnotation annotation = context.getContentProperty().getField().getAnnotation(StringToBooleanConverterAnnotation.class);

            //判断该注解是否存在
            if(annotation != null){

                //读取待写入的布尔值
                boolean booleanValue = context.getValue();
                if(booleanValue){

                    // 将为true时的字符串反写到Excel
                    result = annotation.trueStr();
                }else{

                    // 将为false时的字符串反写到Excel
                    result = annotation.falseStr();
                }
            }else{
                log.error("写入时使用自定义字符串转布尔值转换器，缺少配套注解@StringToBooleanConverterAnnotation");
            }

        }catch (Exception e){
            log.error("将数据写入Excle出现异常。Exception={}",e);
        }
        return new WriteCellData<>(result);
    }
}
