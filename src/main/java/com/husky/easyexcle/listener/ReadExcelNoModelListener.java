package com.husky.easyexcle.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.ehcache.impl.internal.classes.commonslang.reflect.ConstructorUtils;

import java.lang.reflect.Constructor;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @Author husky
 * @Date 2023/2/21 14:17
 * @Description: 不创建实体类的 读取Excel 监听器
 **/
@Slf4j
public class ReadExcelNoModelListener implements ReadListener<Map<String, Object>> {
    /**
     * 每隔51条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private final int BATCH_COUNT = 51;
    /**
     * 消费没有返回值的函数式接口
     */
    private final Consumer<List<Map<String, Object>>> consumer;

    public ReadExcelNoModelListener(Consumer<List<Map<String, Object>>> consumer) {
        this.consumer = consumer;
    }

    /**
     * 缓存的数据
     */
    private List<Map<String, Object>> dataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    /**
     * 这个每一条数据解析都会来调用
     * @param noModelData Map对象 读取 Excel
     * @param analysisContext 获取Excel上下文对象
     */
    @Override
    public void invoke(Map<String, Object> noModelData, AnalysisContext analysisContext) {
        // 放入结果集
        dataList.add(noModelData);

        //  不超过咱们期望的数据集大小
        if(dataList.size() >= BATCH_COUNT){
            consumer.accept(dataList);
            dataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * 数据解析完成调用
     * @param analysisContext 获取Excel上下文对象
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 判断是否还有添加数据，保证所有数据全部添加
        if(CollectionUtils.isNotEmpty(dataList)){
            consumer.accept(dataList);
        }
    }
}
