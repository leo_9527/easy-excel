package com.husky.easyexcle.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.enums.CellExtraTypeEnum;
import com.alibaba.excel.enums.WriteDirectionEnum;
import com.alibaba.excel.metadata.data.*;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.util.MapUtils;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.alibaba.excel.write.metadata.fill.FillWrapper;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.fastjson.JSON;
import com.husky.easyexcle.EasyExcelApplication;
import com.husky.easyexcle.entity.*;
import com.husky.easyexcle.interceptor.CustomCellWriteHandler;
import com.husky.easyexcle.interceptor.CustomRowWriteHandler;
import com.husky.easyexcle.interceptor.CustomSheetWriteHandler;
import com.husky.easyexcle.listener.*;
import com.husky.easyexcle.service.ReadExcelBasicService;
import com.sun.javafx.collections.MappingChange;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.SocketHandler;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author husky
 * @Date 2023/2/17 09:14
 * @Description: 常用注解
 *
 * @Test 😗*表示方法是测试方法。但是与JUnit4的@Test不同，他的职责非常单一不能声明任何属性，拓展的测试将会由Jupiter提供额外测试
 * @ParameterizedTest 😗*表示方法是参数化测试，
 * @RepeatedTest 😗*表示方法可重复执行
 * @DisplayName 😗*为测试类或者测试方法设置展示名称
 * @BeforeEach 😗*表示在每个单元测试之前执行
 * @AfterEach 😗*表示在每个单元测试之后执行
 * @BeforeAll 😗*表示在所有单元测试之前执行
 * @AfterAll 😗*表示在所有单元测试之后执行
 * @Tag 😗*表示单元测试类别，类似于JUnit4中的@Categories
 * @Disabled 😗*表示测试类或测试方法不执行，类似于JUnit4中的@Ignore
 * @Timeout 😗*表示测试方法运行如果超过了指定时间将会返回错误
 * @ExtendWith 😗*为测试类或测试方法提供扩展类引用
 */
@Slf4j
@SpringBootTest(classes={EasyExcelApplication.class})
class ExcleUtilTest {

    /**
     * 基础导出数据
     */
    @Test
    void writeExcelBasicEntity() {
        List<WriteExcelBasicEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelBasicEntity w = new WriteExcelBasicEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }

        // 不写出的字段
        List<String> notShow = new CopyOnWriteArrayList<>();
        notShow.add("dateData");  // dateData -> 实体类中的属性名称
        notShow.add("stringData");  // dateData -> 实体类中的属性名称

        EasyExcel.write(getWritePath()+"writeExcelBasicEntity.xlsx", WriteExcelBasicEntity.class)
                // .excludeColumnFieldNames(notShow)
                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
        // ExcleUtil.exportExcle("writeExcelBasicEntity.xlsx","sheet1",list,WriteExcelBasicEntity.class);

    }

    /**
     * 分配列号导出
     */
    @Test
    void writeExcelAssignIndexEntity() {
        List<WriteExcelAssignIndexEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelAssignIndexEntity w = new WriteExcelAssignIndexEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }
        EasyExcel.write(getWritePath()+"writeExcelAssignIndexEntity.xlsx", WriteExcelAssignIndexEntity.class)

                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
    }

    /**
     * 复杂表头导出
     */
    @Test
    void WriteExcelComplexHeadEntity() {
        List<WriteExcelComplexHeadEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelComplexHeadEntity w = new WriteExcelComplexHeadEntity();
            w.setStringData1("字符串"+i);
            w.setStringData2("字符串"+i);
            w.setDateData1(LocalDateTime.now().plusMinutes(i));
            w.setDateData2(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }
        EasyExcel.write(getWritePath()+"WriteExcelComplexHeadEntity.xlsx", WriteExcelComplexHeadEntity.class)

                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
    }

    /**
     * 自定义、转换相关导出
     */
    @Test
    void WriteExcelConverterEntity() {
        List<WriteExcelConverterEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelConverterEntity w = new WriteExcelConverterEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            w.setBooleanData(i%2 == 0);
            dataList.add(w);
        }
        EasyExcel.write(getWritePath()+"WriteExcelConverterEntity.xlsx", WriteExcelConverterEntity.class)

                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
    }

    /**
     * 复杂单元格相关导出
     */
    @Test
    void WriteExcelComplexContentEntity() {
        List<WriteExcelComplexContentEntity> dataList = new CopyOnWriteArrayList<>();
        WriteExcelComplexContentEntity w = new WriteExcelComplexContentEntity();

        // 设置超链接
        WriteCellData<String> hyperlink = new WriteCellData<>("官方文档");
        HyperlinkData hyperlinkData = new HyperlinkData();
        hyperlink.setHyperlinkData(hyperlinkData);
        hyperlinkData.setAddress("https://gitee.com/leo_9527/easy-excel");
        hyperlinkData.setHyperlinkType(HyperlinkData.HyperlinkType.URL);
        w.setHyperlink(hyperlink);// 超链接

        // 设置备注
        WriteCellData<String> comment = new WriteCellData<>("设置备注");

        CommentData commentData = new CommentData();
        comment.setCommentData(commentData);
        commentData.setAuthor("husky");
        commentData.setRichTextStringData(new RichTextStringData("这是一个备注"));
        // 备注的默认大小是按照单元格的大小 这里想调整到4个单元格那么大 所以向后 向下 各额外占用了一个单元格
        // commentData.setRelativeLastColumnIndex(1);
        // commentData.setRelativeLastRowIndex(1);

        w.setCommentData(comment);// 备注

        // 设置公式
        WriteCellData<String> formula = new WriteCellData<>();
        FormulaData formulaData = new FormulaData();
        formula.setFormulaData(formulaData);
        // 将 123456789 中的第一个数字替换成 2
        // 这里只是例子 如果真的涉及到公式 能内存算好尽量内存算好 公式能不用尽量不用
        formulaData.setFormulaValue("REPLACE(123456789,1,1,2)");
        w.setFormulaData(formula);//公式

        // 设置单个单元格的样式 当然样式 很多的话 也可以用注解等方式。
        WriteCellData<String> writeCellStyle = new WriteCellData<>("单元格样式");
        writeCellStyle.setType(CellDataTypeEnum.STRING);

        WriteCellStyle writeCellStyleData = new WriteCellStyle();
        writeCellStyle.setWriteCellStyle(writeCellStyleData);
        // 这里需要指定 FillPatternType 为FillPatternType.SOLID_FOREGROUND 不然无法显示背景颜色.
        writeCellStyleData.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
        // 背景绿色
        writeCellStyleData.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        w.setWriteCellStyle(writeCellStyle);// 指定单元格的样式 等同于注解
        // 设置单个单元格多种样式
        // 这里需要设置 inMomery=true 不然会导致无法展示单个单元格多种样式，所以慎用
        WriteCellData<String> richTest = new WriteCellData<>();
        richTest.setType(CellDataTypeEnum.RICH_TEXT_STRING);
        RichTextStringData richTextStringData = new RichTextStringData();
        richTest.setRichTextStringDataValue(richTextStringData);
        richTextStringData.setTextString("红色绿色默认");
        // 前2个字红色
        WriteFont writeFont = new WriteFont();
        writeFont.setColor(IndexedColors.RED.getIndex());
        richTextStringData.applyFont(0, 2, writeFont);
        // 接下来2个字绿色
        writeFont = new WriteFont();
        writeFont.setColor(IndexedColors.GREEN.getIndex());
        richTextStringData.applyFont(2, 4, writeFont);
        w.setRichText(richTest);// 指定一个单元格有多个样式

        dataList.add(w);
        EasyExcel.write(getWritePath()+"WriteExcelComplexContentEntity.xlsx", WriteExcelComplexContentEntity.class)
                .inMemory(true)
                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
    }

    /**
     * 样式体验
     */
    @Test
    void WriteExcelStyleEntity() {
        List<WriteExcelStyleEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelStyleEntity w = new WriteExcelStyleEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }
        EasyExcel.write(getWritePath()+"WriteExcelStyleEntity.xlsx", WriteExcelStyleEntity.class)
                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
        // ExcleUtil.exportExcle("writeExcelBasicEntity.xlsx","sheet1",list,WriteExcelBasicEntity.class);

    }

    /**
     * 单元格合并
     */
    @Test
    void WriteExcelLoopMergeEntity() {
        List<WriteExcelLoopMergeEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelLoopMergeEntity w = new WriteExcelLoopMergeEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }
        EasyExcel.write(getWritePath()+"WriteExcelLoopMergeEntity.xlsx", WriteExcelLoopMergeEntity.class)
                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                .table()
                .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
        // ExcleUtil.exportExcle("writeExcelBasicEntity.xlsx","sheet1",list,WriteExcelBasicEntity.class);

    }

    /**
     * 动态头，实时生成头写入
     * <p>
     * 思路是这样子的，先创建List<String>头格式的sheet仅仅写入头,然后通过table 不写入头的方式 去写入数据
     *
     * <p>
     * 1. 创建excel对应的实体对象 参照{@link WriteExcelBasicEntity}
     * <p>
     * 2. 然后写入table即可
     */
    @Test
    public void dynamicHeadWrite() {
        List<WriteExcelBasicEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelBasicEntity w = new WriteExcelBasicEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }

        EasyExcel.write(getWritePath()+"WriteExcelDynamicHead.xlsx")
                // 这里放入动态头
                .head(head())
                .sheet("动态表头")
                // 当然这里数据也可以用 List<List<String>> 去传入
                .doWrite(dataList);
    }
    private List<List<String>> head() {
        List<List<String>> list = new ArrayList<List<String>>();
        List<String> head0 = new ArrayList<String>();
        head0.add("字符串标题" + System.currentTimeMillis());
        List<String> head1 = new ArrayList<String>();
        head1.add("数字标题" + System.currentTimeMillis());
        List<String> head2 = new ArrayList<String>();
        head2.add("日期标题" + System.currentTimeMillis());
        list.add(head0);
        list.add(head1);
        list.add(head2);
        return list;
    }

    /**
     * 根据模板写入
     */
    @Test
    void writeExcelTemplateEntity() {
        List<WriteExcelAssignIndexEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelAssignIndexEntity w = new WriteExcelAssignIndexEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }

        String templateFileName = getTemplatePath("write") + "templateWriteExcelAssignIndexEntity.xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 这里要注意 withTemplate 的模板文件会全量存储在内存里面，所以尽量不要用于追加文件，如果文件模板文件过大会OOM
        // 如果要再文件中追加（无法在一个线程里面处理，可以在一个线程的建议参照多次写入的demo） 建议临时存储到数据库 或者 磁盘缓存(ehcache) 然后再一次性写入
        EasyExcel.write(getWritePath()+"templateWriteExcelAssignIndexEntity.xlsx", WriteExcelAssignIndexEntity.class)
                .withTemplate(templateFileName)
                .needHead(false)
                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet(0)

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                // .table()
                // .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    // 分页查询数据
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
        // ExcleUtil.exportExcle("writeExcelBasicEntity.xlsx","sheet1",list,WriteExcelBasicEntity.class);

    }

    /**
     * 写出时增加自定义拦截器
     */
    @Test
    void customHandlerWrite() {
        List<WriteExcelBasicEntity> dataList = new CopyOnWriteArrayList<>();
        for(int i=1;i<100;i++){
            WriteExcelBasicEntity w = new WriteExcelBasicEntity();
            w.setStringData("字符串"+i);
            w.setDateData(LocalDateTime.now().plusMinutes(i));
            w.setDoubleData(new Random().nextInt(1000));
            dataList.add(w);
        }
        EasyExcel.write(getWritePath()+"customHandlerWriteExcelBasicEntity.xlsx", WriteExcelBasicEntity.class)
                .registerWriteHandler(new CustomCellWriteHandler())
                .registerWriteHandler(new CustomSheetWriteHandler())
                .registerWriteHandler(new CustomRowWriteHandler())
                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                // .table()
                // .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    log.info("待导出数据={}",dataList);

                    return dataList;
                });
    }

    /**
     * 不创建对象的写
     */
    @Test
    public void noModelWrite() {
        List<List<Object>> dataList = ListUtils.newArrayList();
        for (int i = 0; i < 100; i++) {
            List<Object> data = ListUtils.newArrayList();
            data.add("字符串" + i);
            data.add(LocalDateTime.now().plusMinutes(i));
            data.add(new Random().nextInt(1000));
            dataList.add(data);
        }

        EasyExcel.write(getWritePath()+"noModelWrite.xlsx")
                .head(head())

                // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
                .sheet("模板1")

                // 在 sheet 方法之后， 在 doWrite方法之前都是设置WriteSheet的参数
                // .table()
                // .needHead(false) //默认为true，回出现两行表头
                // 在 table 方法之后， 在 doWrite方法之前都是设置WriteTable的参数
                .doWrite(() -> {
                    log.info("待导出数据={}",dataList);
                    return dataList;
                });
    }

// --------------------------------分割线--------------------------------------

    @Autowired
    private ReadExcelBasicService readExcelBasicService;

    /**
     * 基础读取Excel
     */
    @Test
    void readExcelBasicEntity() {

        String fileName = getWritePath()+"writeExcelBasicEntity.xlsx";
        EasyExcel.read(fileName, ReadExcelBasicEntity.class, new ReadExcelBasicListener(readExcelBasicService))
                .ignoreEmptyRow(true) //忽略空的行
                // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
                .sheet(0,"基础读取Excel")
                // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
                .doRead();

    }

    /**
     * 指定列的下标或者列名 读取Excel
     */
    @Test
    void readExcelIndexOrNameEntity() {


        String fileName = getWritePath()+"writeExcelBasicEntity.xlsx";
        EasyExcel.read(fileName, ReadExcelIndexOrNameEntity.class, new ReadExcelIndexOrNameListener(dataList -> {

            log.info("指定列的下标或者列名 Excel内容{}", JSON.toJSONString(dataList));
        }))
        // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
        .sheet(0,"指定列的下标或者列名读取Excel")
        // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
        .doRead();

    }

    /**
     * 复杂表头 读取Excel
     */
    @Test
    void readExcelComplexHeadEntity() {


        String fileName = getWritePath()+"WriteExcelComplexHeadEntity.xlsx";
        EasyExcel.read(fileName, ReadExcelComplexHeadEntity.class, new ReadExcelComplexHeadListener(dataList -> {

            log.info("复杂表头 Excel内容{}", JSON.toJSONString(dataList));
        }))
                // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
                .sheet(0,"复杂表头读取Excel")
                // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
                .doRead();

    }

    /**
     * 额外信息（批注、超链接、合并单元格信息） 读取 Excel
     */
    @Test
    void ReadExcelExtraEntity() {
        String fileName = getWritePath()+"WriteExcelComplexContentEntity.xlsx";
        EasyExcel.read(fileName, ReadExcelExtraEntity.class, new ReadExcelExtraListener(dataList -> {

            log.info("额外信息（批注、超链接、合并单元格信息） Excel内容{}", JSON.toJSONString(dataList));
        }))
                // 需要读取批注 默认不读取
                .extraRead(CellExtraTypeEnum.COMMENT)
                // 需要读取超链接 默认不读取
                .extraRead(CellExtraTypeEnum.HYPERLINK)
                // 需要读取合并单元格信息 默认不读取
                .extraRead(CellExtraTypeEnum.MERGE)
                // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
                .sheet(0,"额外信息")
                // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
                .doRead();

    }

    /**
     * 不创建对象 读取Excel
     */
    @Test
    void ReadExcelNoModel() {


        String fileName = getWritePath()+"WriteExcelComplexHeadEntity.xlsx";
        EasyExcel.read(fileName, new ReadExcelNoModelListener(dataList -> {

            log.info("不创建对象 Excel内容{}", JSON.toJSONString(dataList));
        }))
                // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
                .sheet(0,"不创建对象读取Excel")
                // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
                .doRead();

    }

// --------------------------------分割线--------------------------------------

    /**
     * 单行模板填充
     */
    @Test
    void singleRowFillTemplate() {

        // 写出文件名
        String fileName = getWritePath() + "singleRowFill.xlsx";

        // 读取模板
        String templateFileName = getTemplatePath("read")+"fillTemplate.xlsx";
        // 待填充数据
        Map<String, Object> map = new ConcurrentHashMap<>(16);
        map.put("name", "猪悟能");
        map.put("age", 18);
        map.put("address", "高老庄");

        EasyExcel.write(fileName)
                .withTemplate(templateFileName)
                // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
                .sheet(0)
                // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
                .doFill(map);
    }

    /**
     * 列表模板填充
     */
    @Test
    void fillListTemplate() {

        // 写出文件名
        String fileName = getWritePath() + "fillList.xlsx";

        /**
         * 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
         * {} 代表普通变量 {.} 代表是list的变量
         */
        String templateFileName = getTemplatePath("read")+"fillTemplate.xlsx";

        // 待填充数据
        List<FillExcelListEntity> dateList = new CopyOnWriteArrayList<>();
        for(int i=1;i<=100;i++){
            FillExcelListEntity fillExcelListEntity = new FillExcelListEntity();
            fillExcelListEntity.setAddress("中山路"+i+"号院");
            fillExcelListEntity.setAge(i);
            fillExcelListEntity.setName("张"+i+"元");
            dateList.add(fillExcelListEntity);
        }

        EasyExcel.write(fileName)
                .withTemplate(templateFileName)
                // 在 read 方法之后， 在 sheet方法之前都是设置ReadWorkbook的参数
                .sheet(1)
                // 在 sheet 方法之后， 在 doRead方法之前都是设置ReadSheet的参数
                .doFill(() ->{
                    return dateList;
                });
    }

    /**
     * 复杂模板填充
     */
    @Test
    public void complexFill() {

        // 写出文件名
        String fileName = getWritePath() + "complexFill.xlsx";

        /**
         * 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
         * {} 代表普通变量 {.} 代表是list的变量
         */
        // String templateFileName = getTemplatePath("read")+"complexFillTemplate.xlsx";
        String templateFileName = getTemplatePath("read")+"fillTemplate.xlsx";
        FillExcelComplexEntity fillExcelComplexEntity = new FillExcelComplexEntity();
        fillExcelComplexEntity.setTotalScore(new BigDecimal("0"));
        // 待填充数据
        List<FillExcelComplexListDateEntity> dateList = new CopyOnWriteArrayList<>();
        for(int i=1;i<=15;i++){
            FillExcelComplexListDateEntity complexListDate = new FillExcelComplexListDateEntity();
            BigDecimal score = new BigDecimal(new Random().nextInt(90) + i);
            complexListDate.setScore(score);
            complexListDate.setGrade(i);
            complexListDate.setName("张"+i+"元");
            dateList.add(complexListDate);
            fillExcelComplexEntity.setTotalScore(fillExcelComplexEntity.getTotalScore().add(score));
        }
        fillExcelComplexEntity.setDateTime(LocalDateTime.now());
        fillExcelComplexEntity.setDateList(dateList);

        try (ExcelWriter excelWriter = EasyExcel.write(fileName).withTemplate(templateFileName).build()) {
            WriteSheet writeSheet = EasyExcel.writerSheet(2).build();
            // 这里注意 入参用了forceNewRow 代表在写入list的时候不管list下面有没有空行 都会创建一行，然后下面的数据往后移动。默认 是false，会直接使用下一行，如果没有则创建。
            // forceNewRow 如果设置了true,有个缺点 就是他会把所有的数据都放到内存了，所以慎用
            // 简单的说 如果你的模板有list,且list不是最后一行，下面还有数据需要填充 就必须设置 forceNewRow=true 但是这个就会把所有数据放到内存 会很耗内存
            // 如果数据量大 list不是最后一行 参照 数据量大复杂数据填充
            FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            excelWriter.fill(fillExcelComplexEntity.getDateList(), fillConfig, writeSheet);

            excelWriter.fill(fillExcelComplexEntity, writeSheet);
        }
    }

    /**
     * 大数据量 复杂模板填充  [PS：目前没理解，先模拟官方贴示]
     */
    @Test
    public void volumeComplexFill() {

        // 写出文件名
        String fileName = getWritePath() + "volumeComplexFill.xlsx";

        /**
         * 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
         * {} 代表普通变量 {.} 代表是list的变量
         */
        String templateFileName = getTemplatePath("read")+"complexFillTemplate.xlsx";

        BigDecimal totalScore = new BigDecimal("0");
        // 待填充数据
        List<FillExcelComplexListDateEntity> dateList = new CopyOnWriteArrayList<>();
        for(int i=1;i<=15;i++){
            FillExcelComplexListDateEntity complexListDate = new FillExcelComplexListDateEntity();
            BigDecimal score = new BigDecimal(new Random().nextInt(90) + i);
            complexListDate.setScore(score);
            complexListDate.setGrade(i);
            complexListDate.setName("张"+i+"元");
            dateList.add(complexListDate);
            totalScore = totalScore.add(score);
        }

        // 方案1
        try (ExcelWriter excelWriter = EasyExcel.write(fileName).withTemplate(templateFileName).build()) {
            WriteSheet writeSheet = EasyExcel.writerSheet(2).build();
            // 直接写入数据
            excelWriter.fill(dateList, writeSheet);
            excelWriter.fill(dateList, writeSheet);

            // 写入list之前的数据
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("date", LocalDateTime.now());
            excelWriter.fill(map, writeSheet);

            // list 后面还有个统计 想办法手动写入
            // 这里偷懒直接用list 也可以用对象
            List<List<String>> totalListList = ListUtils.newArrayList();
            List<String> totalList = ListUtils.newArrayList();
            totalListList.add(totalList);
            totalList.add(null);
            totalList.add(null);
            totalList.add(null);
            // 第四列
            totalList.add("统计:"+totalScore);
            // 这里是write 别和fill 搞错了
            excelWriter.write(totalListList, writeSheet);
            // 总体上写法比较复杂 但是也没有想到好的版本 异步的去写入excel 不支持行的删除和移动，也不支持备注这种的写入，所以也排除了可以
            // 新建一个 然后一点点复制过来的方案，最后导致list需要新增行的时候，后面的列的数据没法后移，后续会继续想想解决方案
        }
    }

    /**
     * 横向填充
     */
    @Test
    public void horizontalFill() {

        // 写出文件名
        String fileName = getWritePath() + "horizontalFill.xlsx";

        /**
         * 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
         * {} 代表普通变量 {.} 代表是list的变量
         */
        String templateFileName = getTemplatePath("read")+"fillTemplate.xlsx";
        // 待填充数据
        List<FillExcelListEntity> dateList = new CopyOnWriteArrayList<>();
        for(int i=1;i<=100;i++){
            FillExcelListEntity fillExcelListEntity = new FillExcelListEntity();
            fillExcelListEntity.setAddress("中山路"+i+"号院");
            fillExcelListEntity.setAge(i);
            fillExcelListEntity.setName("张"+i+"元");
            dateList.add(fillExcelListEntity);
        }

        try (ExcelWriter excelWriter = EasyExcel.write(fileName).withTemplate(templateFileName).build()) {
            WriteSheet writeSheet = EasyExcel.writerSheet(3).build();

            // WriteDirectionEnum.HORIZONTAL 水平写
            FillConfig fillConfig = FillConfig.builder().direction(WriteDirectionEnum.HORIZONTAL).build();
            excelWriter.fill(dateList, fillConfig, writeSheet);
        }
    }

    /**
     * 复合填充
     */
    @Test
    public void compositeFill() {

        // 写出文件名
        String fileName = getWritePath() + "compositeFill.xlsx";

        /**
         * 模板注意 用{} 来表示你要用的变量 如果本来就有"{","}" 特殊字符 用"\{","\}"代替
         * {} 代表普通变量 {.} 代表是list的变量
         */
        String templateFileName = getTemplatePath("read")+"fillTemplate.xlsx";
        // 待填充数据
        List<FillExcelListEntity> dateList = new CopyOnWriteArrayList<>();
        for(int i=1;i<=100;i++){
            FillExcelListEntity fillExcelListEntity = new FillExcelListEntity();
            fillExcelListEntity.setAddress("长安街"+i+"号院");
            fillExcelListEntity.setAge(i);
            fillExcelListEntity.setName("张"+i+"丰");
            dateList.add(fillExcelListEntity);
        }

        // 复杂填充对象
        BigDecimal totalScore = new BigDecimal("0");
        // 待填充数据
        List<FillExcelComplexListDateEntity> dateList1 = new CopyOnWriteArrayList<>();
        for(int i=1;i<=15;i++){
            FillExcelComplexListDateEntity complexListDate = new FillExcelComplexListDateEntity();
            BigDecimal score = new BigDecimal(new Random().nextInt(90) + i);
            complexListDate.setScore(score);
            complexListDate.setGrade(i);
            complexListDate.setName("张"+i+"元");
            dateList1.add(complexListDate);
            totalScore = totalScore.add(score);
        }
        try (ExcelWriter excelWriter = EasyExcel.write(fileName).withTemplate(templateFileName).build()) {
            WriteSheet writeSheet = EasyExcel.writerSheet(4).build();

            FillConfig fillConfig2 = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
            excelWriter.fill(new FillWrapper("complexFill", dateList1),fillConfig2, writeSheet);
            // excelWriter.fill(new FillWrapper("complexFill", dateList1),fillConfig2, writeSheet);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("dateTime", LocalDateTime.now());
            map.put("totalScore", totalScore);
            excelWriter.fill(map, writeSheet);

            // WriteDirectionEnum.HORIZONTAL 水平写
            FillConfig fillConfig = FillConfig.builder().direction(WriteDirectionEnum.HORIZONTAL).build();
            excelWriter.fill(dateList, fillConfig, writeSheet);
            excelWriter.fill(dateList, writeSheet);
            // excelWriter.fill(new FillWrapper("horizontal", dateList), fillConfig, writeSheet);
            excelWriter.fill(new FillWrapper("horizontal", dateList), fillConfig, writeSheet);
            excelWriter.fill(new FillWrapper("fillList", dateList), writeSheet);
            // excelWriter.fill(new FillWrapper("fillList", dateList), writeSheet);
        }
    }


    // --------------------------------分割线--------------------------------------


    @Test
    void exportExcle() {
        getTemplatePath("write");
    }

    /**
     * 获取写出文件目录
     * @return
     */
    private String getWritePath(){
        //获取根目录
        ApplicationHome ah = new ApplicationHome();

        //excle 导出文件夹
        String excelPath = ah.getDir().getPath()+"/excel/write/";

        //根据年月日创建文件夹
        String timePath = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).replace("-","/")+"/";
        //如果没有文件夹则新建
        File filePath = new File(excelPath  + timePath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        log.info("excelPath={}",excelPath + timePath);
        return excelPath  + timePath;
    }

    /**
     * 获取 读取文件目录
     * @return
     */
    private static String getReadPath(String modelPath){
        if(!modelPath.endsWith("/")){
            modelPath = modelPath+"/";
        }
        //获取根目录
        ApplicationHome ah = new ApplicationHome();

        //excle 读取文件夹
        String excelPath = ah.getDir().getPath()+"/excel/read/";


        //如果没有文件夹则新建
        File filePath = new File(excelPath  + modelPath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        log.info("excelPath={}",excelPath + modelPath);
        return excelPath  + modelPath;
    }
    /**
     * 获取 模板文件目录
     * @return
     */
    private String getTemplatePath(String typePath){
        if(!typePath.endsWith("/")){
            typePath = typePath+"/";
        }
        //获取根目录
        ApplicationHome ah = new ApplicationHome();

        //excle 导出文件夹
        String excelPath = ah.getDir().getPath()+"/excel/template/";

        //根据年月日创建文件夹
        // String timePath = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).replace("-","/")+"/";
        //如果没有文件夹则新建
        File filePath = new File(excelPath + typePath );
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        log.info("excelPath={}",excelPath + typePath );
        return excelPath  + typePath ;
    }
}